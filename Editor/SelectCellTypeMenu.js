﻿

var selectCellTypeMenuWidth = 850;
var selectCellTypeMenuHeight = 500;

var objectsSelectCellType = [];


function CreateSelectCellTypeMenu (left, top, scale, values, onEnd) {
	
	DestroySelectCellTypeMenu ();
	
	var backgroundDiv = СreateDiv("", left, top, selectCellTypeMenuWidth, selectCellTypeMenuHeight, ()=>{});
	backgroundDiv.style.border = "1px solid";
	backgroundDiv.style.backgroundColor = "#FFFFFF";
	backgroundDiv.style.opacity = 0.95;
	
	objectsSelectCellType.push (backgroundDiv);
	
	var deltaWidth = (selectCellTypeMenuWidth - 20) / 3;
	var deltaHeight = (selectCellTypeMenuHeight - 20) / 3;
	
    objectsSelectCellType.push (CreateTableCell ("SelectCellTypeMenu00", left + 35 + deltaWidth * 0
	, top + 20 + deltaHeight * 0, "Full", CreateConcatenatedArray  (values)
	, [1, 1, 1, 1], 300 * 3 / 4, 100 * 3 / 4)); 
    objectsSelectCellType.push (CreateTableCell ("SelectCellTypeMenu01", left + 35 + deltaWidth * 1
	, top + 20 + deltaHeight * 0, "TopBottom", CreateConcatenatedArray  (values)
	, [1, 1, 1, 1], 300 * 3 / 4, 100 * 3 / 4)); 
    objectsSelectCellType.push (CreateTableCell ("SelectCellTypeMenu02", left + 35 + deltaWidth * 2
	, top + 20 + deltaHeight * 0, "TopBottom2", CreateConcatenatedArray  (values)
	, [1, 1, 1, 1], 300 * 3 / 4, 100 * 3 / 4)); 	
	
	
    objectsSelectCellType.push (CreateTableCell ("SelectCellTypeMenu10", left + 35 + deltaWidth * 0
	, top + 20 + deltaHeight * 1, "Top2Bottom2", CreateConcatenatedArray  (values)
	, [1, 1, 1, 1], 300 * 3 / 4, 100 * 3 / 4)); 
    objectsSelectCellType.push (CreateTableCell ("SelectCellTypeMenu11", left + 35 + deltaWidth * 1
	, top + 20 + deltaHeight * 1, "Top2Bottom", CreateConcatenatedArray  (values)
	, [1, 1, 1, 1], 300 * 3 / 4, 100 * 3 / 4)); 
    objectsSelectCellType.push (CreateTableCell ("SelectCellTypeMenu12", left + 35 + deltaWidth * 2
	, top + 20 + deltaHeight * 1, "LeftRight", CreateConcatenatedArray  (values)
	, [1, 1, 1, 1], 300 * 3 / 4, 100 * 3 / 4)); 	
	
	
    objectsSelectCellType.push (CreateTableCell ("SelectCellTypeMenu20", left + 35 + deltaWidth * 0
	, top + 20 + deltaHeight * 2, "LeftRight2", CreateConcatenatedArray  (values)
	, [1, 1, 1, 1], 300 * 3 / 4, 100 * 3 / 4)); 
    objectsSelectCellType.push (CreateTableCell ("SelectCellTypeMenu21", left + 35 + deltaWidth * 2
	, top + 20 + deltaHeight * 2, "Left2Right", CreateConcatenatedArray  (values)
	, [1, 1, 1, 1], 300 * 3 / 4, 100 * 3 / 4)); 
    
	objectsSelectCellType [1].onclick = () => {
		
		if (values.length > 1) {
			
			CreateMessageBox (values.length - 1 > 1 ? "Останні " + (values.length - 1) + " події будуть видалені" : "Остання подія буде видалена"
				, [
				
					["Гаразд", () => {
						
						while (values.length > 1) { RemoveEventFromCalendarRaw (values [values.length - 1] [3]);  values.pop (); }
						DestroySelectCellTypeMenu ();
						onEnd ("Full");
					}]
					, ["Скасувати", () => {
						
						DestroySelectCellTypeMenu ();
					}]
				])
		} else {
			
			
			DestroySelectCellTypeMenu ();
			onEnd ("Full");
		}
		
	};
	objectsSelectCellType [2].onclick = () => {
		
		if (values.length > 2) {
			
			CreateMessageBox (values.length - 1 > 2 ? "Останні " + (values.length - 2) + " події будуть видалені" : "Остання подія буде видалена"
				, [
				
					["Гаразд", () => {
						
						while (values.length > 2) { RemoveEventFromCalendarRaw (values [values.length - 1] [3]);  values.pop (); }
						DestroySelectCellTypeMenu ();
						onEnd ("TopBottom");
					}]
					, ["Скасувати", () => {
						
						DestroySelectCellTypeMenu ();
					}]
				])
		} else {
			
			
			DestroySelectCellTypeMenu ();
			onEnd ("TopBottom");
		}
	};
	objectsSelectCellType [3].onclick = () => {
		
		if (values.length > 3) {
			
			CreateMessageBox (values.length - 1 > 3 ? "Останні " + (values.length - 3) + " події будуть видалені" : "Остання подія буде видалена"
				, [
				
					["Гаразд", () => {
						
						while (values.length > 3) { RemoveEventFromCalendarRaw (values [values.length - 1] [3]);  values.pop (); }
						DestroySelectCellTypeMenu ();
						onEnd ("TopBottom2");
					}]
					, ["Скасувати", () => {
						
						DestroySelectCellTypeMenu ();
					}]
				])
		} else {
			
			
			DestroySelectCellTypeMenu ();
			onEnd ("TopBottom2");
		}
	};
	objectsSelectCellType [4].onclick = () => {
		
		if (values.length > 4) {
			
			CreateMessageBox (values.length - 1 > 4 ? "Останні " + (values.length - 4) + " події будуть видалені" : "Остання подія буде видалена"
				, [
				
					["Гаразд", () => {
						
						while (values.length > 4) { RemoveEventFromCalendarRaw (values [values.length - 1] [3]);  values.pop (); }
						DestroySelectCellTypeMenu ();
						onEnd ("Top2Bottom2");
					}]
					, ["Скасувати", () => {
						
						DestroySelectCellTypeMenu ();
					}]
				])
		} else {
			
			
			DestroySelectCellTypeMenu ();
			onEnd ("Top2Bottom2");
		}
	};
	objectsSelectCellType [5].onclick = () => {
		
		if (values.length > 3) {
			
			CreateMessageBox (values.length - 1 > 3 ? "Останні " + (values.length - 3) + " події будуть видалені" : "Остання подія буде видалена"
				, [
				
					["Гаразд", () => {
						
						while (values.length > 3) { RemoveEventFromCalendarRaw (values [values.length - 1] [3]);  values.pop (); }
						DestroySelectCellTypeMenu ();
						onEnd ("Top2Bottom");
					}]
					, ["Скасувати", () => {
						
						DestroySelectCellTypeMenu ();
					}]
				])
		} else {
			
			
			DestroySelectCellTypeMenu ();
			onEnd ("Top2Bottom");
		}
	};
	objectsSelectCellType [6].onclick = () => {
		
		if (values.length > 2) {
			
			CreateMessageBox (values.length - 1 > 2 ? "Останні " + (values.length - 2) + " події будуть видалені" : "Остання подія буде видалена"
				, [
				
					["Гаразд", () => {
						
						while (values.length > 1) { RemoveEventFromCalendarRaw (values [values.length - 1] [3]);  values.pop (); }
						DestroySelectCellTypeMenu ();
						onEnd ("LeftRight");
					}]
					, ["Скасувати", () => {
						
						DestroySelectCellTypeMenu ();
					}]
				])
		} else {
			
			
			DestroySelectCellTypeMenu ();
			onEnd ("LeftRight");
		}
	};
	objectsSelectCellType [7].onclick = () => {
		
		if (values.length > 3) {
			
			CreateMessageBox (values.length - 1 > 3 ? "Останні " + (values.length - 3) + " події будуть видалені" : "Остання подія буде видалена"
				, [
				
					["Гаразд", () => {
						
						while (values.length > 3) { RemoveEventFromCalendarRaw (values [values.length - 1] [3]);  values.pop (); }
						DestroySelectCellTypeMenu ();
						onEnd ("LeftRight2");
					}]
					, ["Скасувати", () => {
						
						DestroySelectCellTypeMenu ();
					}]
				])
		} else {
			
			
			DestroySelectCellTypeMenu ();
			onEnd ("LeftRight2");
		}
	};
	objectsSelectCellType [8].onclick = () => {
		
		if (values.length > 3) {
			
			CreateMessageBox (values.length - 1 > 3 ? "Останні " + (values.length - 3) + " події будуть видалені" : "Остання подія буде видалена"
				, [
				
					["Гаразд", () => {
						
						while (values.length > 3) { RemoveEventFromCalendarRaw (values [values.length - 1] [3]);  values.pop (); }
						DestroySelectCellTypeMenu ();
						onEnd ("Left2Right");
					}]
					, ["Скасувати", () => {
						
						DestroySelectCellTypeMenu ();
					}]
				])
		} else {
			
			
			DestroySelectCellTypeMenu ();
			onEnd ("Left2Right");
		}
	};
	
	for (var i = 0; i < objectsSelectCellType.length; i++) {
	
		objectsSelectCellType [i].style.position = "fixed";
	}
}


function DestroySelectCellTypeMenu () {
	
	for (var i = 0; i < objectsSelectCellType.length; i++) {
		
		DeleteObject (objectsSelectCellType [i]);
	}
	
	
	objectsSelectCellType = [];
}





























