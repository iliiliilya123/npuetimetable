

var editListsMenuWidth = 850;
var editListsMenuHeight = 500;

var objectsEditListsMenu = [];

var editListsType;

function OnDelClick (left, top, scale, listToEdit, name, onNewItem, onDelete, onEnd, i) {
	
	return () => {
		
				onDelete (listToEdit [i], (newToEdit) => {
					
					DestroyEditListsMenu ();
					CreateEditListsMenu (left, top, scale, newToEdit, name, onNewItem, onDelete, onEnd);
				});
			};
	
}

function CreateEditListsMenu (left, top, scale, listToEdit, name, onNewItem, onDelete, onEnd) {
	
	DestroyEditListsMenu ();
	
	editListsMenuHeight = 300 + listToEdit.length * 40;
	
	var backgroundDiv = СreateDiv("", left, top, editListsMenuWidth, editListsMenuHeight, ()=>{});
	backgroundDiv.style.border = "1px solid";
	backgroundDiv.style.backgroundColor = "#FFFFFF";
	backgroundDiv.style.opacity = 0.95;
	
	objectsEditListsMenu.push (backgroundDiv);
	
	for (var i = 0; i < listToEdit.length; i++) {
		
		objectsEditListsMenu.push (СreateDivInput("", "EditListsMenu0_" + i, left + editListsMenuWidth / 2 - 200
			, top + 100 + i * 40, 400, 50, 20, "EditListsMenu" + i, ""));
		
		SetValue ("EditListsMenu0_" + i, listToEdit [i] [1]);
		
		
		var delDiv = СreateDiv("Видалити", left + editListsMenuWidth / 2 + 100, top + 100 + i * 40, 200, 20
			
			, OnDelClick (left, top, scale, listToEdit, name, onNewItem, onDelete, onEnd, i)
			, "TextElement"
		);
		delDiv.style.fontSize = 18 * (isMobile ? mobileScale : 1) + "px";
		delDiv.style.border = "1px solid";
		
		AddChangingColor (delDiv, "#FFFFFF", "#FF0000");
		delDiv.style.boxShadow = "grey 2px 2px";
		objectsEditListsMenu.push (delDiv);
	}
	
	var descDiv = СreateDiv(name, left + editListsMenuWidth / 2 - 100, top + 20, 200, 30, () => {}, "TextElement");
	descDiv.style.fontSize = 25 * (isMobile ? mobileScale : 1) + "px";
	descDiv.style.border = "1px solid";
	
	objectsEditListsMenu.push (descDiv);
	
	var newDiv = СreateDiv("Додати", left + editListsMenuWidth / 2 - 100, top + editListsMenuHeight - 120, 200, 40
		, () => {
			
			for (var i = 0; i < listToEdit.length; i++) {
				
				listToEdit [i] [1] = GetValue ("EditListsMenu0_" + i);
			}
			
			console.log (["listToEdit", listToEdit]);
			
			onNewItem (() => {
				
				DestroyEditListsMenu ();
				CreateEditListsMenu (left, top, scale, listToEdit, name, onNewItem, onDelete, onEnd);
			});
		}
		, "TextElement"
	);
	
	newDiv.style.fontSize = 30 * (isMobile ? mobileScale : 1) + "px";
	newDiv.style.border = "1px solid";
	newDiv.style.boxShadow = "grey 2px 2px";
	
	objectsEditListsMenu.push (newDiv);
	
	AddChangingColor (newDiv, "#FFFFFF", "#00FF00");
	
	var okDiv = СreateDiv("Застосувати", left + editListsMenuWidth / 2 - 100, top + editListsMenuHeight - 60, 200, 40
		, () => {
			
			for (var i = 0; i < listToEdit.length; i++) {
				
				listToEdit [i] [1] = GetValue ("EditListsMenu0_" + i);
				if (listToEdit [i] [1] == "") {
					
					listToEdit [i] [1] = " ";
				}
			}
			
			DestroyEditListsMenu ();
			onEnd ();
		}
		, "TextElement"
	);
	
	okDiv.style.fontSize = 30 * (isMobile ? mobileScale : 1) + "px";
	okDiv.style.border = "1px solid";
	okDiv.style.boxShadow = "grey 2px 2px";
	
	AddChangingColor (okDiv, "#FFFFFF", "#00FF00");
	
	objectsEditListsMenu.push (okDiv);
	
	SetBackgroundSize (0, Math.max (backgroundHeight, editListsMenuHeight + top + 50));
}


function DestroyEditListsMenu () {
	
	for (var i = 0; i < objectsEditListsMenu.length; i++) {
		
		DeleteObject (objectsEditListsMenu [i]);
	}
	
	
	objectsEditListsMenu = [];
}





























