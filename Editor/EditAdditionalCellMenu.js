﻿

var editAdditionalCellMenuWidth = 850;
var editAdditionalCellMenuHeight = 500;

var objectsAdditionalCellMenu = [];


function CreateEditAdditionalCellMenu (left, top, scale, targetId, onEnd) {
	
	DestroyEditAdditionalCellMenu ();
	
	var backgroundDiv = СreateDiv("", left, top, editAdditionalCellMenuWidth, editAdditionalCellMenuHeight, ()=>{});
	backgroundDiv.style.border = "1px solid";
	backgroundDiv.style.backgroundColor = "#FFFFFF";
	backgroundDiv.style.opacity = 0.95;
	
	objectsAdditionalCellMenu.push (backgroundDiv);
	
	var type = "Null";
	var topText = "";
	var groupId = -1;
	
	if (targetId.indexOf ("ExamsCell") > -1) {
		
		type = 0;
		topText = "Екзамени";
		groupId = targetId.replace ("ExamsCell", "");
	}
	
	if (targetId.indexOf ("TestsCell") > -1) {
		
		type = 1;
		topText = "Заліки";
		groupId = targetId.replace ("TestsCell", "");
	}
	
	if (targetId.indexOf ("PracticsCell") > -1) {
		
		type = 2;
		topText = "Практики";
		groupId = targetId.replace ("PracticsCell", "");
	}
	
	console.log (["type, groupId", type, groupId]);
	console.log (["GetAdditionalCalendarData", GetAdditionalCalendarData (groupId)]);
	
	var data = GetAdditionalCalendarData (groupId);
	
	var descDiv = СreateDiv(topText, left + editAdditionalCellMenuWidth / 2 - 100, top + 20, 200, 30, () => {}, "TextElement");
	descDiv.style.fontSize = 25 * (isMobile ? mobileScale : 1) + "px";
	descDiv.style.border = "1px solid";
	objectsAdditionalCellMenu.push (descDiv);
	
	objectsAdditionalCellMenu.push (СreateDivTextArea(data [type + 1],"EditAdditionalCellsPairsCount0", left + editAdditionalCellMenuWidth / 2 - 200, top + 150, 400, 100, "PairsCount00"));
	
	var okDiv = СreateDiv("Застосувати", left + editAdditionalCellMenuWidth / 2 - 100, top + 320, 200, 40
		, ()=>{
			
			data [type + 1] = GetValue ("EditAdditionalCellsPairsCount0");
			SetAdditionalCalendarData (data);
			onEnd ();
			DestroyEditAdditionalCellMenu ();
		}
		, "TextElement"
	);
	
	okDiv.style.fontSize = 30 * (isMobile ? mobileScale : 1) + "px";
	okDiv.style.border = "1px solid";
	
	AddChangingColor (okDiv, "#FFFFFF", "#00FF00");
	
	objectsAdditionalCellMenu.push (okDiv);
	
	for (var i = 0; i < objectsAdditionalCellMenu.length; i++) {
	
		objectsAdditionalCellMenu [i].style.position = "fixed";
	}
}


function DestroyEditAdditionalCellMenu () {
	
	for (var i = 0; i < objectsAdditionalCellMenu.length; i++) {
		
		DeleteObject (objectsAdditionalCellMenu [i]);
	}
	
	
	objectsAdditionalCellMenu = [];
}





























