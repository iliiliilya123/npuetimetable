﻿
var saveDiv;
var setGoogleCalendarsDiv;
var daySelect;

var daySelectStartX;
var daySelectStartY;

var deltaXMenu, deltaYMenu;

var isNeedToSetGoogleCalendars;

function CheckGoogleCalendars () {
	
	isNeedToSetGoogleCalendars = false;
	
	for (var i = 0; i < lectorsListRaw.length; i++) {
		
		if (lectorsListRaw [i] [2] == "" || lectorsListRaw [i] [2].indexOf ("Error") > -1) {
			
			isNeedToSetGoogleCalendars = true;
		}
	}
	
	for (var i = 0; i < groupsListRaw.length; i++) {
		
		if (groupsListRaw [i] [2] == "" || groupsListRaw [i] [2].indexOf ("Error") > -1) {
			
			isNeedToSetGoogleCalendars = true;
		}
	}
	
	for (var i = 0; i < auditoriesListRaw.length; i++) {
		
		if (auditoriesListRaw [i] [2] == "" || auditoriesListRaw [i] [2].indexOf ("Error") > -1) {
			
			isNeedToSetGoogleCalendars = true;
		}
	}
	
	if (isNeedToSetGoogleCalendars) {
		
		setGoogleCalendarsDiv.style.backgroundColor = "#AAFFAA";
	} else {
		
		setGoogleCalendarsDiv.style.backgroundColor = "#FFFFFF";
	}
}

function EditLectors () {
			
	CreateEditListsMenu (screenLeftPosition + (window.innerWidth - editListsMenuWidth) / 2, screenTopPosition + 200, 1, lectorsListRaw, "Викладачі"
		
		, (onEnd) => {
			
			lectorsListRaw.push ([-1, "", ""]);
			
			onEnd ();
		}
		, (toDelete, onEnd) => {
			
			lectorsListRaw = RemoveElement (lectorsListRaw, toDelete);
			
			onEnd (lectorsListRaw);
		}
		, () => {
			
			CreateMessageBox ("Зберігаємо зміни");
			SetLectorsToServer (JSON.stringify (lectorsListRaw)
			
				, (s) => {
					
					console.log (s);
					
					GetLectorsFromServer ((res) => {
						
						lectorsListRaw = res;
						
						GetNameLists ();
						GetCalendar (DateToString (new Date ()));
						UpdateFilterLists (1, deltaXMenu, deltaYMenu + 100);
						UpdateFilters ();
						CheckGoogleCalendars ();
						CreateMessageBox ("Збережено", [["Гаразд", () => {}]]);
					});
					
				}
				, (e) => {
					
					CreateMessageBox ("Помилка!<br>" + e, [["Гаразд", () => {}]]);
				}
			);
		}
	);
}

function EditGroups () {
			
	CreateEditListsMenu (screenLeftPosition + (window.innerWidth - editListsMenuWidth) / 2, screenTopPosition + 200, 1, groupsListRaw, "Групи"
		
		, (onEnd) => {
			
			groupsListRaw.push ([-1, "", ""]);
			
			onEnd ();
		}
		, (toDelete, onEnd) => {
			
			groupsListRaw = RemoveElement (groupsListRaw, toDelete);
			
			onEnd (groupsListRaw);
		}
		, () => {
			
			CreateMessageBox ("Зберігаємо зміни");
			SetGroupsToServer (JSON.stringify (groupsListRaw)
			
				, (s) => {
					
					console.log (s);
					
					GetGroupsFromServer ((res) => {
						
						groupsListRaw = res;
						
						GetNameLists ();
						GetCalendar (DateToString (new Date ()));
						UpdateFilterLists (1, deltaXMenu, deltaYMenu + 100);
						UpdateFilters ();
						CheckGoogleCalendars ();
						CreateMessageBox ("Збережено", [["Гаразд", () => {}]]);
					});
					
				}
				, (e) => {
					
					CreateMessageBox ("Помилка!<br>" + e, [["Гаразд", () => {}]]);
				}
			);
		}
	);
}

function EditAuditories () {
			
	CreateEditListsMenu (screenLeftPosition + (window.innerWidth - editListsMenuWidth) / 2, screenTopPosition + 200, 1, auditoriesListRaw, "Аудиторії"
		
		, (onEnd) => {
			
			auditoriesListRaw.push ([-1, "", ""]);
			
			onEnd ();
		}
		, (toDelete, onEnd) => {
			
			auditoriesListRaw = RemoveElement (auditoriesListRaw, toDelete);
			
			onEnd (auditoriesListRaw);
		}
		, () => {
			
			CreateMessageBox ("Зберігаємо зміни");
			SetAuditoriesToServer (JSON.stringify (auditoriesListRaw)
			
				, (s) => {
					
					console.log (s);
					
					GetAuditoriesFromServer ((res) => {
						
						auditoriesListRaw = res;
						
						GetNameLists ();
						GetCalendar (DateToString (new Date ()));
						UpdateFilterLists (1, deltaXMenu, deltaYMenu + 100);
						UpdateFilters ();
						CheckGoogleCalendars ();
						CreateMessageBox ("Збережено", [["Гаразд", () => {}]]);
					});
					
				}
				, (e) => {
					
					CreateMessageBox ("Помилка!<br>" + e, [["Гаразд", () => {}]]);
				}
			);
		}
	);
}

function DrawMenu (scale, deltaX, deltaY) {
	
	deltaXMenu = deltaX;
	deltaYMenu = deltaY;
	
	lectorsListDiv = СreateDiv("Список викладачів", deltaX + 500, deltaY + 50, 200, 23
		, EditLectors
		, "TextElement"
	);
	
	lectorsListDiv.style.fontSize = 18 * (isMobile ? mobileScale : 1) + "px";
	lectorsListDiv.style.border = "1px solid";
	lectorsListDiv.style.boxShadow = "grey 2px 2px";
	AddChangingColor (lectorsListDiv, "#FFFFFF", "#0000FF");
	
	groupsListDiv = СreateDiv("Список груп", deltaX + 750, deltaY + 50, 200, 23
		, EditGroups
		, "TextElement"
	);
	
	groupsListDiv.style.fontSize = 18 * (isMobile ? mobileScale : 1) + "px";
	groupsListDiv.style.border = "1px solid";
	groupsListDiv.style.boxShadow = "grey 2px 2px";
	AddChangingColor (groupsListDiv, "#FFFFFF", "#0000FF");
	
	auditoriesListDiv = СreateDiv("Список аудиторій", deltaX + 1000, deltaY + 50, 200, 23
		, EditAuditories
		, "TextElement"
	);
	
	auditoriesListDiv.style.fontSize = 18 * (isMobile ? mobileScale : 1) + "px";
	auditoriesListDiv.style.border = "1px solid";
	auditoriesListDiv.style.boxShadow = "grey 2px 2px";
	AddChangingColor (auditoriesListDiv, "#FFFFFF", "#0000FF");
	
	DrawFilters (scale, deltaX, deltaY + 100);
	
	saveDiv = СreateDiv("Зберегти", deltaX + 250, deltaY + 50, 200, 23
		, () => {
			
			CreateMessageBox ("Зберігаємо зміни");
			SetCalendarRawToServer (JSON.stringify (calendarRaw)
			
				, (s) => {
					
					console.log (["1+", s]);
					
					SetCalendarAdditionalToServer (JSON.stringify (calendarAdditional)
					
						, (s) => {
							
							console.log (s);
							CreateMessageBox ("Збережено", [["Гаразд", () => {}]]);
						}
						, (e) => {
							
							CreateMessageBox ("Помилка!<br>2 " + e, [["Гаразд", () => {}]]);
						}
					);
				}
				, (e) => {
					
					CreateMessageBox ("Помилка!<br>1 " + e, [["Гаразд", () => {}]]);
				}
			);
		}
		, "TextElement"
	);
	
	saveDiv.style.fontSize = 18 * (isMobile ? mobileScale : 1) + "px";
	saveDiv.style.border = "1px solid";
	//saveDiv.style.position = "fixed";
	saveDiv.style.zIndex = 10;
	saveDiv.style.boxShadow = "grey 2px 2px";
	
	AddChangingColor (saveDiv, "#FFFFFF", "#00FF00");
	
	daySelect = СreateDivCalendar("", "Calendar0", deltaX, deltaY + 50, 200, 24, 200, "Calendar00", "GetCalendar (GetValue ('Calendar0'));UpdateFilters ();");
	
	daySelectStartX = deltaX;
	daySelectStartY = deltaY + 50;
	//daySelect.style.position = "fixed";
	daySelect.style.zIndex = 10;
	
	setGoogleCalendarsDiv = СreateDiv("Створити календарі", deltaX + 1250, deltaY + 50, 200, 23
		, () => {
			
			CreateMessageBox ("Створюємо календарі");
			
			SetGoogleCalendars ((res) => {
				
				GetLectorsFromServer ((res1) => {
			
					lectorsListRaw = res1;
					
					GetAuditoriesFromServer ((res2) => {
						
						auditoriesListRaw = res2;
						
						GetGroupsFromServer ((res3) => {
							
							if (res == "Done") {
								
								CreateMessageBox ("Створено", [["Гаразд", () => {}]]);
								isNeedToSetGoogleCalendars = false;
								setGoogleCalendarsDiv.style.backgroundColor = "#FFFFFF";
							} else {
								
								CreateMessageBox ("Перевищено ліміт створеннь Google-календарів.<br>Спробуйте ще раз через годину.", [["Гаразд", () => {}]]);
								isNeedToSetGoogleCalendars = true;
								setGoogleCalendarsDiv.style.backgroundColor = "#AAFFAA";
							}
						});
					});
				});
			});
		}
		, "TextElement"
	);
	
	setGoogleCalendarsDiv.style.fontSize = 18 * (isMobile ? mobileScale : 1) + "px";
	setGoogleCalendarsDiv.style.border = "1px solid";
	setGoogleCalendarsDiv.style.boxShadow = "grey 2px 2px";
	
	AddChangingColor (setGoogleCalendarsDiv, "#FFFFFF", "#00FF00");
	
	SetValue ("Calendar0", DateToString (new Date ()));
	
	SetBackgroundSize (Math.max (backgroundWidth, deltaX + 1250 + 260 + 20), 0);
}

function OnCellClick (trg) {
			
	if (isNeedToSetGoogleCalendars) {
		
		CreateMessageBox ("Перед редагуванням розкладу створіть всі календарі", [["Гаразд", () => {}]]);
		return;
	}		
			
	if (lectorsListRaw.length == 0) {
		
		CreateMessageBox ("Список викладачів пустий!", [["Редагувати", () => {EditLectors ();}], ["Скасувати", () => {}]]);
		return;
	}	

	if (auditoriesListRaw.length == 0) {
		
		CreateMessageBox ("Список аудиторій пустий!", [["Редагувати", () => {EditAuditories ();}], ["Скасувати", () => {}]]);
		return;
	}		
			
	var toClickId;
	
	if (trg.target.parentNode.id == "main") {
		
		toClickId = trg.target.id;
	} else {
		
		toClickId = trg.target.parentNode.id
	}	
			
	CreateEditCellMenu ((window.innerWidth - editCellMenuWidth) / 2, 100, 1, cellParametersById [toClickId], () => {
		
		redrawCalendar ();
	});
			
}

function OnAdditionalCellClick (trg) {
		
	var toClickId;
	
	if (trg.target.parentNode.id == "main") {
		
		toClickId = trg.target.id;
	} else {
		
		toClickId = trg.target.parentNode.id
	}	
			
	console.log (["toClickId", toClickId]);
	
	CreateEditAdditionalCellMenu ((window.innerWidth - editAdditionalCellMenuWidth) / 2, 100, 1, toClickId, () => {
		
		redrawCalendar ();
	});
			
}

function Main (faculties) {
	
	GetNameLists ();
	GetCalendar (DateToString (new Date ()));
	
	facultyName = "";
	
	for (var i = 0; i  < faculties.length; i++) {
		
		if (faculties [i] [0] == faculty) {
			
			facultyName = faculties [i] [1];
		}
	}
	
	facultyNameDiv = СreateDiv("<a style='margin-left:40px; line-height:1.6;'>" + facultyName + "</a>", 0, 0, 18000, 100);
	facultyNameDiv.style.background = "#7E8A8B";
	facultyNameDiv.style.fontSize = 60 * (isMobile ? mobileScale : 1) + "px";
	facultyNameDiv.style.color = "#FFFFFF";
	
	
	DrawMenu (1, 100, 100);
	
	СreateDiv("", 100 - 20, 300 - 51, 10, 10, () => {}, "calendarTable");
	backgroundCalendarLeft = 100 - 20;
	backgroundCalendarTop = 300 - 51;
	
	DrawCalendar (1, 20, 51, 9, OnCellClick, OnAdditionalCellClick);
	
	window.addEventListener('scroll', () => {
		
		screenLeftPosition = (window.pageXOffset || document.documentElement.scrollLeft) - (document.documentElement.clientLeft || 0);
		screenTopPosition = (window.pageYOffset || document.documentElement.scrollTop)  - (document.documentElement.clientTop || 0);	
		
		if (daySelectStartX - 10 < screenLeftPosition) {
			
			daySelect.style.left = screenLeftPosition + 10 + "px";
			saveDiv.style.left = screenLeftPosition + 260 + "px";
		} else {
			
			daySelect.style.left = daySelectStartX + "px";
			saveDiv.style.left = daySelectStartX + 250 + "px";
		}
		
		if (daySelectStartY - 10 < screenTopPosition) {
			
			daySelect.style.top = screenTopPosition + 10 + "px";
			saveDiv.style.top = screenTopPosition + 10 + "px";
		} else {
			
			daySelect.style.top = daySelectStartY + "px";
			saveDiv.style.top = daySelectStartY + "px";
		}
		
	}, false);
	
	CheckGoogleCalendars ();
	
	console.log ([oddEvenDay, isOddDay]);
}

function OnFacultyClick (clickLink) {
	
	return () => {
				
		OpenLink (clickLink);
	}
}

function CreateFacultyMenu (faculties) {
	
	var topDiv = СreateDiv("<a style='margin-left:40px; line-height:1.6;'>" + "НПУ Редактор розкладу" + "</a>", 0, 0, 18000, 100);
	topDiv.style.background = "#7E8A8B";
	topDiv.style.fontSize = 60 * (isMobile ? mobileScale : 1) + "px";
	topDiv.style.color = "#FFFFFF";
	
	for (var i = 0; i < faculties.length; i++) {
		
		var clickLink = "?" + faculties [i] [0];
		var description = faculties [i] [1];
		
		facultyDiv = СreateDiv("<br>" + description, window.innerWidth / 2 - 300, 150 + i * 120, 600, 100
			, OnFacultyClick (clickLink)
			, "TextElementRounded"
		);
		
		facultyDiv.style.fontSize = 26 * (isMobile ? mobileScale : 1) + "px";
		facultyDiv.style.border = "1px solid";
		
		
		SetBackgroundSize (0, Math.max (backgroundHeight, 150 + faculties.length * 120));
	}
}

function AfterFacultyLoad (faculties) {
	
	GetCalendarRawFromServer ((res) => {
				
		calendarRaw = res [0];
		calendarAdditional = res [1];
		
		GetLectorsFromServer ((res1) => {
			
			lectorsListRaw = res1;
			
			GetAuditoriesFromServer ((res2) => {
				
				auditoriesListRaw = res2;
				
				GetGroupsFromServer ((res3) => {
					
					groupsListRaw = res3;
					
					Main (faculties);
				});
			});
		});
	});
}

refreshTokenFunction = (onEnd) => {
	
	GetFaculty ((resM1) => {
		
		console.log (resM1);
		
		GetGoogleAuthURL ((res0) => {
				
			authURL = res0;
			
			LoginGoogle ((ress) => {
				
				var emailFirstName = "";
				
				for (var i = 0; i < ress.length && ress [i] != '@'; i++) {
					
					emailFirstName += ress [i];
				}
				
				if (emailFirstName.indexOf("rozklad_") == 0) {
					
					faculty = "";
					
					for (var i = 8; i < emailFirstName.length; i++) {
						
						faculty += emailFirstName [i];
					}
					
					console.log (faculty);
				}
				
				console.log (ress);
				
				onEnd ();
			});
		});
	});
};

refreshTokenFunction (() => {
	
	GetAccessLevel ((lvl) => {
	
		console.log (["GetAccessLevel", lvl]);
		if (lvl < 16) {
			
			CreateMessageBox ("Немає доступу");
			return;
		}
		
		if (faculty.indexOf ("http") > -1) {
			
			GetFacultiesFromServer ((res) => {
				
				CreateFacultyMenu (res); 
			});
		} else {
			
			GetFacultiesFromServer ((res) => {
				
				AfterFacultyLoad (res); 
			});
		}
	});
});
