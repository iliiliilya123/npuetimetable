﻿

var editExamsMenuWidth = 850;
var editExamsMenuHeight = 500;

var objectsExamsMenu = [];


function CreateEditExamsMenu (left, top, scale, inputLink, currentDayDate, checker, onEnd) {
	
	console.log (inputLink);
	
	DestroyEditExamsMenu ();
	
	var backgroundDiv = СreateDiv("", left, top, editExamsMenuWidth, editExamsMenuHeight, ()=>{});
	backgroundDiv.style.border = "1px solid";
	backgroundDiv.style.backgroundColor = "#FFFFFF";
	backgroundDiv.style.opacity = 0.95;
	
	objectsExamsMenu.push (backgroundDiv);
	
	var inputArray = [];
	
	if (inputLink == -1) {
		
		inputArray = ["", "", "", currentDayDate, "", ""]; 
	} else {
		
		var inputRaw = calendarRaw [inputLink];
		
		
		inputArray = [inputRaw [1], GetLectorName (inputRaw [2]), GetAuditoryName (inputRaw [3]), inputRaw [4], inputRaw [5], (inputRaw.length > 9 ? inputRaw [9] : "")];
	}
	
	console.log (inputArray);
	
	var subjectText = inputArray [0];
	var lectorText = inputArray [1];
	var auditoryText = inputArray [2];
	var startTime = inputArray [3];
	var pairsCount = inputArray [4];
	var courseLink = inputArray [5];
	
    objectsExamsMenu.push (СreateDivDatalist("Предмет：　", GetSubjects (), "EditExamss0", left + editExamsMenuWidth / 2 - 300, top + 50, 600, 50, 170, "EditExamss01"));
    objectsExamsMenu.push (СreateDivDatalist("Викладач：　", GetLectors (), "EditExamss1", left + editExamsMenuWidth / 2 - 300, top + 150, 600, 50, 170, "EditExamss11"));
    objectsExamsMenu.push (СreateDivDatalist("Аудиторія：　", GetAuditories (), "EditExamss2", left + editExamsMenuWidth / 2 - 300, top + 250, 600, 50, 170, "EditExamss21"));
	objectsExamsMenu.push (СreateDivCalendar("Початок: ", "EditExamssCalendar1", left + editExamsMenuWidth / 2, top + 50, 350, 20, 200, "Calendar11", ""));
    objectsExamsMenu.push (СreateDivInput("Кількість пар：", "EditExamssPairsCount0", left + editExamsMenuWidth / 2, top + 150, 400, 50, 10, "PairsCount00", ""));
	objectsExamsMenu [objectsExamsMenu.length - 1].childNodes [1].type = "number";
    objectsExamsMenu.push (СreateDivInput("Посилання：", "EditExamss3", left + editExamsMenuWidth / 2, top + 250, 600, 50, 10, "EditExamss31", ""));
	
	SetValue ("EditExamss0", subjectText);
	SetValue ("EditExamss1", lectorText);
	SetValue ("EditExamss2", auditoryText);
	SetValue ("EditExamssCalendar1", startTime);
	SetValue ("EditExamssPairsCount0", pairsCount);
	SetValue ("EditExamss3", courseLink);
	
	var okDiv = СreateDiv("Застосувати", left + editExamsMenuWidth / 2 - 100, top + 320, 200, 40
		, ()=>{
			
			var message = checker ([GetValue ("EditExamss0"), GetValue ("EditExamss1"), GetValue ("EditExamss2")
				, GetValue ("EditExamssCalendar1"), Math.abs (parseInt (GetValue ("EditExamssPairsCount0"))), GetValue ("EditExamss3")]);

			if (message == "Ok") {
				
				onEnd ([GetValue ("EditExamss0"), GetValue ("EditExamss1"), GetValue ("EditExamss2")
					, GetValue ("EditExamssCalendar1"), Math.abs (parseInt (GetValue ("EditExamssPairsCount0"))), GetValue ("EditExamss3")]);
				DestroyEditExamsMenu ();
			} else {
				
				CreateMessageBox (message, [["Гаразд", () => {}]]);
			}
		}
		, "TextElement"
	);
	
	okDiv.style.fontSize = 30 * (isMobile ? mobileScale : 1) + "px";
	okDiv.style.border = "1px solid";
	
	AddChangingColor (okDiv, "#FFFFFF", "#00FF00");
	
	objectsExamsMenu.push (okDiv);
	
	var deleteDiv = СreateDiv("Видалити", left + editExamsMenuWidth / 2 - 100, top + 390, 200, 40
		, ()=>{
			
			onEnd (false);
			DestroyEditExamsMenu ();
		}
		, "TextElement"
	);
	
	deleteDiv.style.fontSize = 30 * (isMobile ? mobileScale : 1) + "px";
	deleteDiv.style.border = "1px solid";
	
	AddChangingColor (deleteDiv, "#FFFFFF", "#FF0000");
	
	objectsExamsMenu.push (deleteDiv);
	
	
	for (var i = 0; i < objectsExamsMenu.length; i++) {
	
		objectsExamsMenu [i].style.position = "fixed";
	}
}


function DestroyEditExamsMenu () {
	
	for (var i = 0; i < objectsExamsMenu.length; i++) {
		
		DeleteObject (objectsExamsMenu [i]);
	}
	
	
	objectsExamsMenu = [];
}





























