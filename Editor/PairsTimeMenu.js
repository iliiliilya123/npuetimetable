﻿

var pairsTimeMenuWidth = 500;
var pairsTimeMenuHeight = 700;

var maxPairs = 10;

var isWrongPairsTime = false;

var objectsPairsTime = [];

function GetStartTime (number) {
	
	switch (number) {
		
		case 0: return "6:30"
		case 1: return "8:00"
		case 2: return "9:30"
		case 3: return "11:00"
		case 4: return "12:30"
		case 5: return "14:00"
		case 6: return "15:30"
		case 7: return "17:00"
		case 8: return "18:30"
		case 9: return "20:00"
		case 10: return "21:30"
	}
	
	return "Undefined";
}

function GetEndTime (number) {
	
	switch (number) {
		
		case 0: return "7:50";
		case 1: return "9:20";
		case 2: return "10:50";
		case 3: return "12:20";
		case 4: return "13:50";
		case 5: return "15:20";
		case 6: return "16:50";
		case 7: return "18:20";
		case 8: return "19:50";
		case 9: return "21:20";
		case 10: return "22:50";
	}
	
	return "Undefined";
}

function PairTimeInMinutes (input) {
	
	var hours = 0;
	var minutes = 0;
	var i = 0;
	
	for (; input [i] != ":" && i < input.length; i++) {
		
		if (input [i] < '0' || input [i] > '9') {
			
			return -1;
		}
		
		hours = hours * 10 + (input [i] - '0');
	}
	
	if (i == input.length - 1) {
		
		return -1;
	}
	
	for (i++; i < input.length; i++) {
		
		if (input [i] < '0' || input [i] > '9') {
			
			return -1;
		}
		
		minutes = minutes * 10 + (input [i] - '0');
	}
	
	return hours * 60 + minutes;
}


function CheckPairsTime () {
	
	isWrongPairsTime = false;
	
	var timePoints = [];
	
	for (var i = 0; i < maxPairs; i++) {
		
		document.getElementById ("underDiv" + i).style.opacity = 0.0;
		
		if (PairTimeInMinutes (GetValue ("startTime" + i)) == -1 || PairTimeInMinutes (GetValue ("endTime" + i)) == -1
			|| (PairTimeInMinutes (GetValue ("startTime" + i)) >= PairTimeInMinutes (GetValue ("endTime" + i)))) {
			
			document.getElementById ("underDiv" + i).style.opacity = 0.7;
			isWrongPairsTime = true;
		} else {
		
			timePoints.push ([PairTimeInMinutes (GetValue ("startTime" + i)), 1, i]);
			timePoints.push ([PairTimeInMinutes (GetValue ("endTime" + i)), 0, i]);
		}
	}
	
	timePoints.sort ((a, b) => {
		
		
		return (a[0] == b[0] ? a[1] - b [1] : a [0] - b [0]);
	});
	
	var sign = 0;
	
	for (var i = 0; i < timePoints.length; i++) {
		
		if (timePoints [i] [1]) {
			
			sign ++;
			
			if (sign > 1) {
				
				isWrongPairsTime = true;
				document.getElementById ("underDiv" + timePoints [i] [2]).style.opacity = 0.7;
				document.getElementById ("underDiv" + timePoints [i - 1] [2]).style.opacity = 0.7;
			}
			
		} else {
			
			sign --;
		}
	}
	
}

function CreatePairsTimeMenu (left, top, scale) {
	
	DestroyPairsTimeMenu ();
	
	var backgroundDiv = СreateDiv("", left, top, pairsTimeMenuWidth, pairsTimeMenuHeight, ()=>{});
	backgroundDiv.style.border = "1px solid";
	backgroundDiv.style.backgroundColor = "#FFFFFF";
	backgroundDiv.style.opacity = 0.95;
	
	objectsPairsTime.push (backgroundDiv);
	
	for (var i = 0; i < maxPairs; i++) {

		var underDiv = СreateDiv("", left + 40, top + 40 + 50 * i, 460, 40, ()=>{}, "underDiv" + i);
		underDiv.style.backgroundColor = "#FF0000";
		underDiv.style.opacity = 0.0;
		var pairDiv = СreateDiv("Пара " + i + ":", left + 50, top + 50 + 50 * i, 100, 20, ()=>{}, "TextElement");
		pairDiv.style.border = "1px solid";
		
		var startTimeDiv = СreateDivInput("Початок: ", "startTime" + i, left + 170, top + 50 + 50 * i, 150, 30, 5, "TextElement", "CheckPairsTime();");
		SetValue ("startTime" + i, GetStartTime (i));
		
		var endTimeDiv = СreateDivInput("Кінець: ", "endTime" + i, left + 320, top + 50 + 50 * i, 150, 30, 5, "TextElement", "CheckPairsTime();");
		SetValue ("endTime" + i, GetEndTime (i));
		
		
		objectsPairsTime.push (underDiv);
		objectsPairsTime.push (pairDiv);
		objectsPairsTime.push (startTimeDiv);
		objectsPairsTime.push (endTimeDiv);
	}
	
	var okDiv = СreateDiv("Застосувати", left + 155, top + 600, 200, 40
		, ()=>{
			
			if (isWrongPairsTime) {
				
				CreateMessageBox (message, [["Неправильний розпорядок дня", () => {}]]);
			} else {
				
				ChangePairsTime ("", () => {
					
					DestroyPairsTimeMenu ();
				});
			} 
		
		}
		, "TextElement"
	);
	
	okDiv.style.fontSize = 30 * (isMobile ? mobileScale : 1) + "px";
	okDiv.style.border = "1px solid";
	
	AddChangingColor (okDiv, "#FFFFFF", "#00FF00");
	
	objectsPairsTime.push (okDiv);
	
	CheckPairsTime ();
}


function DestroyPairsTimeMenu () {
	
	for (var i = 0; i < objectsPairsTime.length; i++) {
		
		DeleteObject (objectsPairsTime [i]);
	}
	
	
	objectsPairsTime = [];
}





























