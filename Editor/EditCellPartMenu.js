﻿

var editCellPartMenuWidth = 850;
var editCellPartMenuHeight = 500;

var objectsCellPartMenu = [];


function CreateEditCellPartMenu (left, top, scale, inputLink, currentDayDate, checker, onEnd) {
	
	console.log (inputLink);
	
	DestroyEditCellPartMenu ();
	
	var backgroundDiv = СreateDiv("", left, top, editCellPartMenuWidth, editCellPartMenuHeight, ()=>{});
	backgroundDiv.style.border = "1px solid";
	backgroundDiv.style.backgroundColor = "#FFFFFF";
	backgroundDiv.style.opacity = 0.95;
	
	objectsCellPartMenu.push (backgroundDiv);
	
	var inputArray = [];
	
	if (inputLink == -1) {
		
		inputArray = ["", "", "", currentDayDate, "", ""]; 
	} else {
		
		var inputRaw = calendarRaw [inputLink];
		
		
		inputArray = [inputRaw [1], GetLectorName (inputRaw [2]), GetAuditoryName (inputRaw [3]), inputRaw [4], inputRaw [5], (inputRaw.length > 9 ? inputRaw [9] : "")];
	}
	
	console.log (inputArray);
	
	var subjectText = inputArray [0];
	var lectorText = inputArray [1];
	var auditoryText = inputArray [2];
	var startTime = inputArray [3];
	var pairsCount = inputArray [4];
	var courseLink = inputArray [5];
	
    objectsCellPartMenu.push (СreateDivDatalist("Предмет：　", GetSubjects (), "EditCellParts0", left + editCellPartMenuWidth / 2 - 300, top + 50, 600, 50, 170, "EditCellParts01"));
    objectsCellPartMenu.push (СreateDivDatalist("Викладач：　", GetLectors (), "EditCellParts1", left + editCellPartMenuWidth / 2 - 300, top + 150, 600, 50, 170, "EditCellParts11"));
    objectsCellPartMenu.push (СreateDivDatalist("Аудиторія：　", GetAuditories (), "EditCellParts2", left + editCellPartMenuWidth / 2 - 300, top + 250, 600, 50, 170, "EditCellParts21"));
	objectsCellPartMenu.push (СreateDivCalendar("Початок: ", "EditCellPartsCalendar1", left + editCellPartMenuWidth / 2, top + 50, 350, 20, 200, "Calendar11", ""));
    objectsCellPartMenu.push (СreateDivInput("Кількість пар：", "EditCellPartsPairsCount0", left + editCellPartMenuWidth / 2, top + 150, 400, 50, 10, "PairsCount00", ""));
	objectsCellPartMenu [objectsCellPartMenu.length - 1].childNodes [1].type = "number";
    objectsCellPartMenu.push (СreateDivInput("Посилання：", "EditCellParts3", left + editCellPartMenuWidth / 2, top + 250, 600, 50, 10, "EditCellParts31", ""));
	
	SetValue ("EditCellParts0", subjectText);
	SetValue ("EditCellParts1", lectorText);
	SetValue ("EditCellParts2", auditoryText);
	SetValue ("EditCellPartsCalendar1", startTime);
	SetValue ("EditCellPartsPairsCount0", pairsCount);
	SetValue ("EditCellParts3", courseLink);
	
	var okDiv = СreateDiv("Застосувати", left + editCellPartMenuWidth / 2 - 100, top + 320, 200, 40
		, ()=>{
			
			var message = checker ([GetValue ("EditCellParts0"), GetValue ("EditCellParts1"), GetValue ("EditCellParts2")
				, GetValue ("EditCellPartsCalendar1"), GetValue ("EditCellPartsPairsCount0"), GetValue ("EditCellParts3")]);

			if (message == "Ok") {
				
				onEnd ([GetValue ("EditCellParts0"), GetValue ("EditCellParts1"), GetValue ("EditCellParts2")
					, GetValue ("EditCellPartsCalendar1"), Math.abs (parseInt (GetValue ("EditCellPartsPairsCount0"))), GetValue ("EditCellParts3")]);
				DestroyEditCellPartMenu ();
			} else {
				
				CreateMessageBox (message, [["Гаразд", () => {}]]);
			}
		}
		, "TextElement"
	);
	
	okDiv.style.fontSize = 30 * (isMobile ? mobileScale : 1) + "px";
	okDiv.style.border = "1px solid";
	
	AddChangingColor (okDiv, "#FFFFFF", "#00FF00");
	
	objectsCellPartMenu.push (okDiv);
	
	var deleteDiv = СreateDiv("Видалити", left + editCellPartMenuWidth / 2 - 100, top + 390, 200, 40
		, ()=>{
			
			onEnd (false);
			DestroyEditCellPartMenu ();
		}
		, "TextElement"
	);
	
	deleteDiv.style.fontSize = 30 * (isMobile ? mobileScale : 1) + "px";
	deleteDiv.style.border = "1px solid";
	
	AddChangingColor (deleteDiv, "#FFFFFF", "#FF0000");
	
	objectsCellPartMenu.push (deleteDiv);
	
	
	for (var i = 0; i < objectsCellPartMenu.length; i++) {
	
		objectsCellPartMenu [i].style.position = "fixed";
	}
}


function DestroyEditCellPartMenu () {
	
	for (var i = 0; i < objectsCellPartMenu.length; i++) {
		
		DeleteObject (objectsCellPartMenu [i]);
	}
	
	
	objectsCellPartMenu = [];
}





























