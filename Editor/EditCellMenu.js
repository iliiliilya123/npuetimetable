﻿

var editCellMenuWidth = 850;
var editCellMenuHeight = 500;

var objectsCellMenu = [];

var editCellType;

var links;

var editCellMenuRedraw = () => {};

function GetDeltaAndSubgroupById (type, id) {
		
	switch (type) {
		
		case "Full": return [0, 0];
		case "TopBottom": return [id == 0 ? 1 : 2, 0];
		case "TopBottom2": if (id == 0) return [1, 0]; if (id == 1) return [2, 1]; return [2, 2];
		case "Top2Bottom2": return [id < 2 ? 1 : 2, id % 2 == 0 ? 1 : 2];
		case "Top2Bottom": if (id == 2) return [2, 0]; if (id == 0) return [1, 1]; return [1, 2];
		case "LeftRight": return [0, id == 0 ? 1 : 2];
		case "LeftRight2": if (id == 0) return [0, 1]; if (id == 1) return [1, 2]; return [2, 2];
		case "Left2Right": if (id == 2) return [0, 2]; if (id == 0) return [1, 1]; return [2, 1];
		
	}
	
	return [-1, -1];
}

function OnCellClickEditMenu (toEdit, cellParameters, left, top, scale, i) {
	
	return (trg) => {
		
			CreateEditCellPartMenu (left, top, scale, links [i], DateToString (cellParameters [7])
			
				, (res) => {
					
					if (GetLectorByName (res [1]) == -1) {
						
						return "Помилка!\nТакого викладача, як " + res [1] + " немає у базі даних.";
					}
					
					if (GetAuditoryByName (res [2]) == -1) {
						
						return "Помилка!\nТакої аудиторії, як " + res [2] + " немає у базі даних.";
					}
					
					if (res [3] == "") {
						
						return "Помилка!\nНеправильна дата.";
					}
					
					if (res [4] == "" || res [4] <= 0) {
						
						return "Помилка!\nНеправильна кількість пар.";
					}
					
					if (oddEvenDay !== false) {
						
						var delta = GetDeltaAndSubgroupById (editCellType, i) [0];
						var distanceToOddEvenDay = DateDistanceInDays (oddEvenDay, GetStartOfTheWeek (new Date (res [3])));
												
						var isMustBeTwoWeeksDistance = (delta == 1 && isOddDay) || (delta == 2 && !isOddDay);
						
						console.log (["distanceToOddEvenDay", distanceToOddEvenDay, delta, isMustBeTwoWeeksDistance]);
						
						if ((distanceToOddEvenDay % 14 != 0) == isMustBeTwoWeeksDistance) {
							
							return "Помилка!\nЦей тиждень є " + (delta == 1 ? "" : "не") + "парним."
						}
					}
					
					return "Ok";
				}			
				, (res) => {
					
					if (res == false) {
						
						RemoveEventFromCalendarRaw (links [i]);
						trg.target.innerHTML = "";
						links [i] = -1;
						saveDiv.style.backgroundColor = "#AAFFAA";
						
						//editCellMenuRedraw ();
						return;
					}
					
					if (GetGroupByName (cellParameters [5]) == -1) {
						
						CreateMessageBox ("Внутрішня помилка. Повідомте розробників", [["Гаразд", () => {}]]);
					} else {
						
						var deltaSubgroup = GetDeltaAndSubgroupById (editCellType, i);
						console.log ([editCellType, links [i], i, deltaSubgroup]);
						
						RemoveEventFromCalendarRaw (links [i]);
						var newLink = AddEventToCalendarRaw ([GetGroupByName (cellParameters [5]), res [0], GetLectorByName (res [1]), GetAuditoryByName (res [2])
							, res [3], res [4], cellParameters [6] + (isUseZeroPair ? 0 : 1), deltaSubgroup [0], deltaSubgroup [1], res [5]]);
							
						trg.target.innerHTML = res [0] + "<br>" + res [1] + "<br>" + res [2];
						links [i] = newLink;
						cellParameters [3] [i] = [res [0], res [1], res [2], newLink];
						
						for (var qs = 0; qs < calendarRaw.length; qs++) {
							
							if (calendarRaw [qs] [1] == res [0]) {
								
								calendarRaw [qs] [9] = res [5];
							} 
						}
						
						saveDiv.style.backgroundColor = "#AAFFAA";
						//editCellMenuRedraw ();
					}
				
				}
			);
		}
}

function SetChildClicks (toEdit, cellParameters, left, top, scale) {
	
	links = [];
    
	for (var i = 0; i < toEdit.childNodes.length; i++) {
		
		links [i] = cellParameters [3].length > i ? cellParameters [3] [i] [3] : -1;
		
		console.log (links [i]);
		var lnkRaw = ReadEventFromCalendarRaw (links [i]); 
		
		console.log (["ECT i dns", editCellType, i, GetDeltaAndSubgroupById (editCellType, i)]);
		
		if (lnkRaw != "null") {
			
			var dns = GetDeltaAndSubgroupById (editCellType, i);
			
			calendarRaw [links [i]] [7] = dns [0];
			calendarRaw [links [i]] [8] = dns [1];
		}
		
		toEdit.childNodes [i].onclick = OnCellClickEditMenu (toEdit, cellParameters, left, top, scale, i);
	}
}

function CreateEditCellMenu (left, top, scale, cellParameters, onEnd) {
	
	DestroyEditCellMenu ();
	
	editCellMenuRedraw = () => { CreateEditCellMenu (left, top, scale, cellParameters, onEnd); };
	
	var backgroundDiv = СreateDiv("", left, top, editCellMenuWidth, editCellMenuHeight, ()=>{});
	backgroundDiv.style.border = "1px solid";
	backgroundDiv.style.backgroundColor = "#FFFFFF";
	backgroundDiv.style.opacity = 0.95;
	
	objectsCellMenu.push (backgroundDiv);
	
	console.log (cellParameters);
	
	var toEdit = CreateTableCell ("EditCellMenu00", left + editCellMenuWidth / 2 - 300 / 2, top + 100, cellParameters [2], CreateConcatenatedArray (cellParameters [3])
		, [1, 1, 1, 1]);
    objectsCellMenu.push (toEdit);
	
	editCellType = cellParameters [2];
	
	SetChildClicks (toEdit, cellParameters, left, top, scale);
	
	var changeDiv = СreateDiv("Змінити тип", left + editCellMenuWidth / 2 - 100, top + 250, 200, 40
		, ()=>{
			
			CreateSelectCellTypeMenu ((window.innerWidth - selectCellTypeMenuWidth) / 2, 100, 1, (cellParameters [3])
				
					, (res) => {
			
					console.log (cellParameters [3]);
					
					DeleteObject (toEdit);	
					
					toEdit = CreateTableCell ("EditCellMenu00", left + editCellMenuWidth / 2 - 300 / 2, top + 100
						, res, CreateConcatenatedArray (cellParameters [3]), [1, 1, 1, 1]);

					editCellType = res; 
					toEdit.style.position = "fixed";
					
					saveDiv.style.backgroundColor = "#AAFFAA";
					
					SetChildClicks (toEdit, cellParameters, left, top, scale);
					
					objectsCellMenu.push (toEdit);
				}
			);
		}
		, "TextElement"
	);
	
	changeDiv.style.fontSize = 30 * (isMobile ? mobileScale : 1) + "px";
	changeDiv.style.border = "1px solid";
	
	AddChangingColor (changeDiv, "#FFFFFF", "#0000FF");
	
	objectsCellMenu.push (changeDiv);
	
	
	var okDiv = СreateDiv("Застосувати", left + editCellMenuWidth / 2 - 100, top + 320, 200, 40
		, () => {
			
			DestroyEditCellMenu ();
			onEnd ();
		}
		, "TextElement"
	);
	
	okDiv.style.fontSize = 30 * (isMobile ? mobileScale : 1) + "px";
	okDiv.style.border = "1px solid";
	
	AddChangingColor (okDiv, "#FFFFFF", "#00FF00");
	
	objectsCellMenu.push (okDiv);
	
	for (var i = 0; i < objectsCellMenu.length; i++) {
	
		objectsCellMenu [i].style.position = "fixed";
	}
}


function DestroyEditCellMenu () {
	
	for (var i = 0; i < objectsCellMenu.length; i++) {
		
		DeleteObject (objectsCellMenu [i]);
	}
	
	
	objectsCellMenu = [];
}





























