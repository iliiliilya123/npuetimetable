

var isMusic;
var isSounds;

var currentMusic;

function StartCurrentMusic() {
	currentMusic();
}

function StopMusic() {
	document.getElementById("freska").pause();
	document.getElementById("freska").currentTime = 0;
	document.getElementById("gameplay").pause();
	document.getElementById("gameplay").currentTime = 0;
	document.getElementById("pobeda").pause();
	document.getElementById("pobeda").currentTime = 0;
	document.getElementById("proigrish").pause();
	document.getElementById("proigrish").currentTime = 0;
	document.getElementById("them").pause();
}

function StartMusicClick() {
	if (!isSounds) return;
	
	document.getElementById("click").play();
}

function StartMusicClock() {
	if (!isSounds) return;
	
		document.getElementById("clock").play();
}
function StartMusicClockUp() {
	if (!isSounds) return;
	
	document.getElementById("clockUP").play();
}

function StartMusicEnergy() {
	if (!isSounds) return;
	
	var audio = new Audio("music/energy.mp3");
		audio.play();
}

function StartMusicFreska() {
	currentMusic = StartMusicFreska;
	if (!isMusic) return;
	
	StopMusicThem();
	document.getElementById("freska").play();
}

function StopMusicFreska() {
	if (currentMusic == StartMusicFreska)
		currentMusic = DoNone;
	document.getElementById("freska").pause();
	document.getElementById("freska").currentTime = 0;
}

function StartMusicFreskaZvuk() {
	if (!isSounds) return;
	
	document.getElementById("freska_zvuk").play();
}
function StopMusicFreskaZvuk() {
	document.getElementById("freska_zvuk").pause();
	document.getElementById("freska_zvuk").currentTime = 0;
}

function StartMusicGameplay() {
	currentMusic = StartMusicGameplay;
	if (!isMusic) return;
	StopMusicThem();
	document.getElementById("gameplay").play();
}

function StopMusicGameplay() {
	if (currentMusic == StartMusicGameplay)
		currentMusic = DoNone;
	document.getElementById("gameplay").pause();
	document.getElementById("gameplay").currentTime = 0;
}

function StartMusicLenta() {
	if (!isSounds) return;
	
	document.getElementById("lenta").play();
}

function StartMusicNaiden() {
	if (!isSounds) return;
	var audio = new Audio("music/naiden.mp3");
		audio.play();
}


function StartMusicPobeda() {
	currentMusic = StartMusicPobeda;
	if (!isMusic) return;
	StopMusicThem();
	document.getElementById("pobeda").play();
}
function StopMusicPobeda() {
	if (currentMusic == StartMusicPobeda)
		currentMusic = DoNone;
	document.getElementById("pobeda").pause();
	document.getElementById("pobeda").currentTime = 0;
}

function StartMusicProigrish() {
	currentMusic = StartMusicProigrish;
	if (!isMusic) return;
	StopMusicThem();
	document.getElementById("proigrish").play();
}
function StopMusicProigrish() {
	if (currentMusic == StartMusicProigrish)
		currentMusic = DoNone;
	document.getElementById("proigrish").pause();
	document.getElementById("proigrish").currentTime = 0;
}

function StartMusicSkill() {
	if (!isSounds) return;
	
	var audio = new Audio("music/skill.mp3");
		audio.play();
}

function StartMusicSvitok() {
	if (!isSounds) return;
	
	document.getElementById("svitok").play();
}

function StartMusicThem() {
	currentMusic = StartMusicThem;
	if (!isMusic) return;
	document.getElementById("them").play();
}

function StopMusicThem() {
	if (currentMusic == StartMusicThem)
		currentMusic = DoNone;
	document.getElementById("them").pause();
	//document.getElementById("them").currentTime = 0;
}

function StartMusicZvezda() {
	if (!isSounds) return;
	
	var audio = new Audio("music/zvezda.mp3");
		audio.play();
}
