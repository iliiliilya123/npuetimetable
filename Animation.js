

function AnimationObject(_drawObject,_framesCount,_time,_path,_extention,_currentFrameIndex) {

	this.drawObject = _drawObject;
	this.framesCount = _framesCount;
	this.time = _time;
	this.path = _path;
	this.extention = _extention;
	this.pause = false;
	this.currentFrameIndex =  _currentFrameIndex;
	
}

AnimationObject.prototype.Pause = function () {
	
	this.pause = true;
	
}

AnimationObject.prototype.Animate = function () {
	
	this._Animate(this);
	
}

AnimationObject.prototype._Animate = function (_this) {
	
	if (_this.pause) 
		return;
	_this.currentFrameIndex++;
	_this.drawObject.src = _this.path +""+ _this.currentFrameIndex +""+ _this.extention;
	
	if (_this.currentFrameIndex >=_this.framesCount)
		_this.currentFrameIndex = 0;
	
	setTimeout(function() {_this.Animate(_this);},_this.time/_this.framesCount);
	
}