
var isFake = false;
var fakeLoginpass = "iliiliilya123@gmail.com|IN3mCZDVMuwrVZMj5HcMWye9cNoqCe8l2bIGamAXR8pM7b5dThZPReZ42ouFCC0GPJl05WYW5nJczOps6piXDh2GGwljalZ04l19hZ6nnPzXC0qKqJI51KMIh72ss1s";

var faculty = "";

var refreshTokenFunction = () => {console.log ("refreshTokenFunction is null");};

function GetCalendarRawFromServer (func) {
 
	if (isFake) {
	
		func ([]); return;
	}
	
	params = [func];
	
	var timeoutFunc = setTimeout(() => { NoConnection("GetCalendarRaw", params); }, 4000);
	
	$.post('../../Server.php' , {"code":"get calendar", "params":"", "loginpass" : loginpass, "faculty" : faculty}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {GetCalendarRawFromServer (func);});
				return;
			}
			
			console.log (["GetCalendarRawFromServer Error", res.Error]);
		}
		
	}, 'json');

}

function GetLectorsFromServer (func) {
 
	if (isFake) {
	
		func ([]); return;
	}
	
	params = [func];
	
	var timeoutFunc = setTimeout(() => { NoConnection("GetLectorsFromServer", params); }, 4000);
	
	$.post('../../Server.php' , {"code":"get lectors", "params":"", "loginpass" : loginpass, "faculty" : faculty}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {GetLectorsFromServer (func);});
				return;
			}
			
			console.log (["GetLectorsFromServer Error", res.Error]);
		}
		
	}, 'json');

}

function GetAuditoriesFromServer (func) {
 
	if (isFake) {
	
		func ([]); return;
	}
	
	params = [func];
	
	var timeoutFunc = setTimeout(() => { NoConnection("GetAuditoriesFromServer", params); }, 4000);
	
	$.post('../../Server.php' , {"code":"get auditories", "params":"", "loginpass" : loginpass, "faculty" : faculty}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {GetAuditoriesFromServer (func);});
				return;
			}
			
			console.log (["GetAuditoriesFromServer Error", res.Error]);
		}
		
	}, 'json');

}

function GetGroupsFromServer (func) {
 
	if (isFake) {
	
		func ([]); return;
	}
	
	params = [func];
	
	var timeoutFunc = setTimeout(() => { NoConnection("GetGroupsFromServer", params); }, 4000);
	
	$.post('../../Server.php' , {"code":"get groups", "params":"", "loginpass" : loginpass, "faculty" : faculty}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {GetGroupsFromServer (func);});
				return;
			}
			
			console.log (["GetGroupsFromServer Error", res.Error]);
		}
		
	}, 'json');

}

function GetFacultiesFromServer (func) {
 
	if (isFake) {
	
		func ([]); return;
	}
	
	params = [func];
	
	var timeoutFunc = setTimeout(() => { NoConnection("GetFacultiesFromServer", params); }, 4000);
	
	$.post('../../Server.php' , {"code":"get faculties", "params":"", "loginpass" : loginpass, "faculty" : "fi"}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {GetFacultiesFromServer (func);});
				return;
			}
			
			console.log (["GetFacultiesFromServer Error", res.Error]);
		}
		
	}, 'json');

}

function GetSettingsFromServer (func) {
 
	if (isFake) {
	
		func ([]); return;
	}
	
	params = [func];
	
	var timeoutFunc = setTimeout(() => { NoConnection("GetSettingsFromServer", params); }, 4000);
	
	$.post('../../Server.php' , {"code":"get settings", "params":"", "loginpass" : loginpass, "faculty" : "fi"}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {GetSettingsFromServer (func);});
				return;
			}
			
			console.log (["GetSettingsFromServer Error", res.Error]);
		}
		
	}, 'json');

}

function GetAccessLevel (func) {
 
	if (isFake) {
	
		func ([]); return;
	}
	
	params = [func];
	
	var timeoutFunc = setTimeout(() => { NoConnection("GetAccessLevel", params); }, 4000);
	
	$.post('../../Server.php' , {"code":"get access level", "params":"", "loginpass" : loginpass, "faculty" : "fi"}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {GetAccessLevel (func);});
				return;
			}
			
			console.log (["GetAccessLevel Error", res.Error]);
		}
		
	}, 'json');

}


function GetGoogleAuthURL (func) {
 
	if (isFake) {
	
		func ([]); return;
	}
	
	params = [func];
	
	var timeoutFunc = setTimeout(() => { NoConnection("GetGoogleAuthURL", params); }, 4000);
	
	$.post('../../GetGoogleAuthURL.php' , {}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {GetGoogleAuthURL (func);});
				return;
			}
			
			console.log (["GetGoogleAuthURL Error", res.Error]);
		}
		
	}, 'json');

}

function SetCalendarRawToServer (value, func, onError) {
 
	if (isFake) {
	
		func (); return;
	}
	
	params = [value, func, onError];
	
	var timeoutFunc = setTimeout(() => { NoConnection("SetCalendarRawToServer", params); }, 60000);
	
	$.post('../../Server.php' , {"code":"set calendar", "params":value, "loginpass" : loginpass, "faculty" : faculty}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {SetCalendarRawToServer (value, func, onError);});
				return;
			}
			
			onError (res.Error);
			console.log (["SetCalendarRawToServer Error", res.Error]);
		}
		
	}, 'json');

}

function SetCalendarAdditionalToServer (value, func, onError) {
 
	if (isFake) {
	
		func (); return;
	}
	
	params = [value, func, onError];
	
	var timeoutFunc = setTimeout(() => { NoConnection("SetCalendarAdditionalToServer", params); }, 60000);
	
	$.post('../../Server.php' , {"code":"set calendar additional", "params":value, "loginpass" : loginpass, "faculty" : faculty}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {SetCalendarAdditionalToServer (value, func, onError);});
				return;
			}
			
			onError (res.Error);
			console.log (["SetCalendarAdditionalToServer Error", res.Error]);
		}
		
	}, 'json');

}

function SetListToServer (type, value, func, onError) {
 
	if (isFake) {
	
		func (); return;
	}
	
	params = [value, func, onError];
	
	var timeoutFunc = setTimeout(() => { NoConnection("SetListToServer", params); }, 60000);
	
	$.post('../../Server.php' , {"code":"set " + type, "params" : value, "loginpass" : loginpass, "faculty" : faculty}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {SetListToServer (type, value, func, onError);});
				return;
			}
			
			onError (res.Error);
			console.log (["SetListToServer Error", res.Error]);
		}
		
	}, 'json');

}

function SetLectorsToServer (value, func, onError) {
	
	SetListToServer ("lectors", value, func, onError);
}

function SetGroupsToServer (value, func, onError) {
	
	SetListToServer ("groups", value, func, onError);
}

function SetAuditoriesToServer (value, func, onError) {
	
	SetListToServer ("auditories", value, func, onError);
}

function SetGoogleCalendars (func) {
 
	if (isFake) {
	
		func ([]); return;
	}
	
	params = [func];
	
	var timeoutFunc = setTimeout(() => { NoConnection("SetGoogleCalendars", params); }, 1000000);
	
	$.post('../../Server.php' , {"code":"set google calendars", "params":"", "loginpass" : loginpass, "faculty" : faculty}, (res) => {
		
		clearTimeout(timeoutFunc);
	 
		if (res.response) {
			
			func (res.response);
		} else {
			
			if (res.Error == "Token") {
				
				refreshTokenFunction (() => {SetGoogleCalendars (func);});
				return;
			}
			
			console.log (["SetGoogleCalendars Error", res.Error]);
		}
		
	}, 'json');

}
